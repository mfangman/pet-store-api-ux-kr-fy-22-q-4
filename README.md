# Welcome to the Pet Store API

This is a demo project to be used for https://gitlab.com/gitlab-org/gitlab-design/-/issues/1702. Relevant details for the heuristic evaluation are included below.  Note that there are no actual API files in this repository. 

## JTBD

> When I am ready to release changes into production, I want to verify it is safe to release, so that I can release the changes responsibly.

## Persona(s)
- [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst)
- [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)


## Scenario
Your engineering team just completed a new Rest API for a project, and until today they haven't run any security assessments against it. To maintain compliance with the organization's security policies, the API must be scanned for security issues before it can be used. The engineering team has deployed the API to a staging environment for testing.

As the team's dedicated AppSec Engineer, you're tasked with testing the staging environment for vulnerabilities.  

### Task
Ensure that the team's new API complies with your organization's security policies. The organization dictates that a running version of the API must be scanned for bugs and potential security issues with 2 different scanning tools.

## API Info
Information about the staging environment can be found below:

### Base URL
https://petstore3.swagger.io/api/v3 _(Note: this will return a 404 when viewed in the browser)_

### OpenAPI Specification 
https://gitlab.com/mfangman/basic-api-ux-kr-fy22-q4/-/blob/main/openapi.json

### Authentication
Authentication is not available for this API
